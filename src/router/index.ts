import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import Home from "@/views/Home.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    name: "main",
    path: "/",
    component: Home
  },
  {
    name: "projects",
    path: "/projects",
    component: () =>
      import(/* webpackChunkName: "projects" */ "../views/Projects.vue")
  },
  {
    name: "projectsHouses",
    path: "/projects/houses",
    component: () =>
      import(/* webpackChunkName: "projects" */ "../views/Projects.vue"),
    props: {
      onlyHousesShown: true
    }
  },
  {
    name: "projectBathHouses",
    path: "/projects/bathhouses",
    component: () =>
      import(/* webpackChunkName: "projects" */ "../views/Projects.vue"),
    props: {
      onlyBathhousesShown: true
    }
  },
  {
    name: "projectCountryHouses",
    path: "/projects/countryhouses",
    component: () =>
      import(/* webpackChunkName: "projects" */ "../views/Projects.vue"),
    props: {
      onlyCountryhousesShown: true
    }
  },
  {
    name: "project",
    path: "/project/:id",
    component: () =>
      import(/* webpackChunkName: "project" */ "../views/Project.vue"),
    meta: {
      stickyHeader: true
    }
  },
  {
    name: "video",
    path: "/video",
    component: () =>
      import(/* webpackChunkName: "video" */ "../views/Video.vue")
  },
  {
    name: "gallery",
    path: "/gallery",
    component: () =>
      import(/* webpackChunkName: "gallery" */ "../views/Gallery.vue")
  },
  {
    name: "objectGallery",
    path: "/gallery/:id",
    component: () =>
      import(
        /* webpackChunkName: "objectGallery" */ "../views/ObjectGallery.vue"
      ),
    meta: {
      stickyHeader: true
    }
  },
  {
    name: "services",
    path: "/services",
    component: () =>
      import(/* webpackChunkName: "services" */ "../views/Services.vue")
  },
  {
    name: "service",
    path: "/service/:id",
    component: () =>
      import(/* webpackChunkName: "services" */ "../views/Article.vue"),
    props: {
      entity: "service"
    },
    meta: {
      stickyHeader: true
    }
  },
  {
    name: "articles",
    path: "/articles",
    component: () =>
      import(/* webpackChunkName: "services" */ "../views/Articles.vue"),
    meta: {
      stickyHeader: true
    }
  },
  {
    name: "article",
    path: "/article/:id",
    component: () =>
      import(/* webpackChunkName: "services" */ "../views/Article.vue"),
    props: {
      entity: "article"
    },
    meta: {
      stickyHeader: true
    }
  },
  {
    name: "contacts",
    path: "/contacts",
    component: () =>
      import(/* webpackChunkName: "services" */ "../views/Contacts.vue")
  }
];

const router = new VueRouter({
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  }
});

export default router;
