import Service from "@/services/service.ts";
import { IProject, IProjects } from "@/typings/types";

export default class ProjectsService extends Service<IProject, IProjects>{
  constructor() {
    super("/project");
  }

  get(id: string): Promise<IProject> {
    return super.get(id);
  }

  getAll(): Promise<IProjects> {
    this.url = `${this.url}s`;
    return super.getAll();
  }
}
