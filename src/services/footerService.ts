import Service from "@/services/service.ts";
import { IFooter } from "@/typings/types";

export default class FooterService extends Service<IFooter, IFooter>{
  constructor() {
    super("/footer");
  }

  async get(): Promise<IFooter> {
    return super.getAll();
  }
}
