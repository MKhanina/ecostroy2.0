import Service from "@/services/service.ts";
import { IService } from "@/typings/types";

export default class ServicesService extends Service<IService, Array<IService>>{
  constructor() {
    super("/service");
  }

  get(id: string): Promise<IService> {
    return super.get(id);
  }

  getAll(): Promise<Array<IService>> {
    this.url = `${this.url}s`;
    return super.getAll();
  }
}
