import Service from "@/services/service.ts";
import { IVideo, IVideoList } from "@/typings/types";

export default class VideoService extends Service<IVideo, IVideoList>{
  constructor() {
    super(`/video`);
  }
}
