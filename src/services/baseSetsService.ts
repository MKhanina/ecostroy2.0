import Service from "@/services/service.ts";
import { ISet } from "@/typings/types";

export default class BaseSetsService extends Service<ISet, Array<ISet>>{
  constructor() {
    super(`/baseSets`);
  }
}
