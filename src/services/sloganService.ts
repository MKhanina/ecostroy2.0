import Service from "@/services/service.ts";
import { ISlogan } from "@/typings/types";

export default class SloganService extends Service<ISlogan, ISlogan> {
  constructor() {
    super("/slogan");
  }

  get(): Promise<ISlogan> {
    return super.getAll();
  }
}
