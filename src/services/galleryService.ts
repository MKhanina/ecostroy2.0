import Service from "@/services/service.ts";
import { IGallery } from "@/typings/types";

export default class GalleryService extends Service<IGallery, Array<IGallery>>{
  constructor() {
    super(`/gallery`);
  }
}
