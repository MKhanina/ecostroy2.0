import Service from "@/services/service.ts";
import { IAbout } from "@/typings/types";

export default class AboutService extends Service<IAbout, IAbout>{
  constructor() {
    super(`/about`);
  }

  get(): Promise<IAbout> {
    return super.getAll();
  }
};
