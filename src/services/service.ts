import Vue from "vue";

export default class Service<T, B> {
  constructor(public url: string) {}

  async getAll(): Promise<B> {
    const { data } = await Vue.$axios.get<B>(this.url);
    return data;
  }

  async get(id: string): Promise<T> {
    const { data } = await Vue.$axios.get<T>(`${this.url}/${id}`);
    return data;
  }

  async post(data:T) {
    Vue.$axios.post(this.url, data);
  }
}
