import Service from "@/services/service.ts";
import { ISet } from "@/typings/types";

export default class AdditionalSetsService extends Service<ISet, Array<ISet>>{
  constructor() {
    super(`/additionalSets`);
  }
}
