import Service from "@/services/service.ts";
import { IContacts } from "@/typings/types";

export default class ContactsService extends Service<IContacts, IContacts>{
  constructor() {
    super("/contacts");
  }

  get(): Promise<IContacts> {
    return super.getAll();
  }
};
