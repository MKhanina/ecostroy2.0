import Service from "@/services/service.ts";
import { IObjectKeys, IRequest } from "@/typings/types";

export default class RequestService extends Service<IRequest, IRequest> {
  constructor(type: string) {
    const urls: IObjectKeys = {
      request: "/request",
      contactUs: "/contactUs"
    }
    super(urls[type]);
  }
};
