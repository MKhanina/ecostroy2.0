import Service from "@/services/service.ts";
import { IArticle } from "@/typings/types";

export default class ArticlesService extends Service<IArticle, Array<IArticle>>{
  constructor() {
    super("/article");
  }

  get(id: string): Promise<IArticle> {
    return super.get(id);
  }

  getAll(): Promise<Array<IArticle>> {
    this.url = `${this.url}s`;
    return super.getAll();
  }
}
