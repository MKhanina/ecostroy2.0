import Vue from "vue";

import { library } from "@fortawesome/fontawesome-svg-core";
import {
  faBars,
  faChevronDown,
  faChevronLeft,
  faChevronRight,
  faChevronUp,
  faEllipsisV,
  faExclamation,
  faFileAlt,
  faImage,
  faInfoCircle,
  faMapMarkerAlt,
  faMinus,
  faPhoneAlt,
  faPlus
} from "@fortawesome/free-solid-svg-icons";
import { faVk, faYoutube } from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

library.add(
  faBars,
  faChevronDown,
  faChevronLeft,
  faChevronRight,
  faChevronUp,
  faEllipsisV,
  faExclamation,
  faFileAlt,
  faImage,
  faInfoCircle,
  faMapMarkerAlt,
  faMinus,
  faPhoneAlt,
  faPlus,

  faVk,
  faYoutube
);

Vue.component("FontAwesomeIcon", FontAwesomeIcon);
