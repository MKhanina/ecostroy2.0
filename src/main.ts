import "./styles/main.scss";

import Vue from "vue";
import "./directives/scroll";
import "./plugins/axios";
import axios from 'axios';
import App from "./App.vue";
import router from "./router";
import "./fontAwesomeIcons";
import VModal from "vue-js-modal";
import Vue2Filters from "vue2-filters";
import VTooltip from "v-tooltip";
import VueFormulate from "@braid/vue-formulate";

Vue.config.productionTip = false;

const axiosInstance = axios.create({
   baseURL: `http://localhost:8080`
 })

Vue.use(VModal);
Vue.use(Vue2Filters);
Vue.use(VTooltip);
Vue.use(VueFormulate, {
  uploader: axiosInstance,
  uploadUrl: '/upload'
});

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
