export type TButton = {
  text: string;
  callback?: () => void;
};

export interface IContacts {
  address: string;
  phones: string[];
  email: string;
}

export interface IObjectKeys {
  [key: string]: string;
}

export interface ISloganTexts extends IObjectKeys {
  main: string;
  projects: string;
  projectsHouses: string;
  projectsBathHouses: string;
  projectsCountryHouses: string;
  video: string;
  gallery: string;
  services: string;
  articles: string;
  contacts: string;
}

export interface ISlogan {
  maintext: TSlogan;
  subtext: Partial<TSlogan>;
  buttontext: Partial<TSlogan>;
}

export interface IFooter {
  text: stirng;
  rights: string;
}

interface IText {
  title: string;
  intro: string;
  text: string;
}

export interface IAbout extends IText {
  button: TButton;
  video: string;
}

export interface IService extends IText {
  id: string;
  image: string;
  content?: string;
}

export interface IFilters {
  houses: boolean;
  bathHouses: boolean;
  countryHouses: boolean;
  onefloor: boolean;
  twofloors: boolean;
  priceFrom: number;
  priceTo: number;
}

export type TFloors = 1 | 2;

export interface IProject {
  id: string;
  title: string;
  type?: string;
  description?: string;
  baseSetsDescription?: string;
  additionalSetsDescription?: string;
  requestDescription?: string;
  image: string;
  images: string[];
  area: number;
  size: number;
  floors: TFloors;
  price: number;
}

export interface IProjects {
  noHouses: string;
  noBathHouses: string;
  noCountryHouses: string;
  houses: IProject[];
  bathHouses: IProject[];
  countryHouses: IProject[];
}

export interface ISetValue {
  id: string;
  name: string;
  info: string;
  images: stirng[];
  video: string;
  price: number;
}
export interface ISet {
  id: string;
  category: string;
  selectedValue: ISetValue;
}

export interface IRequest {
  name: string;
  phone: string;
  email: string;
  comment: string;
  file: string;
  additionalData: any;
}

export interface IVideo {
  id: string;
  video: string;
  title: string;
  text: string;
}

export interface IVideoList {
  newVideo: Array<IVideo>;
  aboutTech: Array<IVideo>;
  review: Array<IVideo>;
}

export interface IGallery {
  id: string;
  title: string;
  text: string;
  image: string;
  images: Array<string>;
}

export interface IArticle extends IText {
  id: string;
  image: string;
  content?: string;
}
