import Vue from "vue";

Vue.directive("scroll", {
  inserted: function(el, binding) {
    const f = function(evt: Event) {
      if (binding.value(evt, el)) {
        window.removeEventListener("scroll", f);
      }
    };
    window.addEventListener("scroll", f);
  }
});
